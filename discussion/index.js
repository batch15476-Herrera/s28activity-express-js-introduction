//[OBJECTIVE] our goal is to create a server-side app using the Express Web Framework.

const express = require("express");
const { get, send } = require("express/lib/response");
//Identify a designated location or address where the connection will be created or served
const port = 4000;

//our objective here is to establish a connection.
//express() -> this creates an express application.
const application = express();

//Assign/bind the connection to the designated desired address or port.
// listen() => this allows us to create a listener on the specified port or location.

//SYNTAX: serverName.listen(port, callback)
    //port => this identifies the port/location where you want to execute the listener.
    //callBack => specifies a function or method that will be executed when the listener has been added or appended.

        //using the callback, let's include a response to the user to verify that the connection has been properly established on the desired port.
application.listen(port, () => console.log(`Express API Server on port: ${port}`));

//==create a response in the main entry point of the server.
//== create a request that will display a message at the base URI which us "/" of the server.

//== When using express, to be able to create a request method type of GET, we simply use a GET method or function.

//SYNTAX: serverName.get(URI/path, callback/method)
    //=> URI/path - this describes the designated path location of route where the request will be sent.
    //=> callback/method - this allows us to identify the process on how the client and server would interact with each other.
application.get('/', (request, response) => {
    //we will describe how the server would interact/respond back to the client.
        //we would need to transmit a message bacak to the client.
        //==> this will allow us to transmit data over a network. 
        response.send('Greetings Welcome to course booking of Therrera');
});

//==section== Setup an environment in our postman client app to simplify collection methods.
    //1. New task is to integrate environment variables to make your work load when testing API collection more easier.