
const express = require("express");
const { get, send } = require("express/lib/response");

const port = 4000;

const application = express();


application.listen(port, () => console.log(`Express API Server on port: ${port}`));


application.get('/', (request, response) => {
    
        response.send('Greetings! Welcome to my to-do-list server.');
});
